﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class Comment
    {
        public int Id { get; set; }
        public FormRevision FormRevision { get; set; }
        //[StringLength(2000),Required]
        public string Text { get; set; }
        public User Creator { get; set; }
        //[StringLength(20),Required]
        //[Column(TypeName = "VARCHAR")]
        public DateTime CreateDateTime { get; set; }
        public CommentType Type { get; set; }
        //[StringLength(1000)]
        public string AttachmentFilePath { get; set; }

    }

    public enum CommentType
    {
        BySection,
        ByProductionCompany,
        ByTpphSupervisor,
    }
}
