﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class Login
    {
        public int LoginId { get; set; }

        //[Required]
        public DateTime Time { get; set; }

        //[StringLength(20)]
        public string IP { get; set; }

       // [Required]
        public bool InOut { get; set; }
    }

}
