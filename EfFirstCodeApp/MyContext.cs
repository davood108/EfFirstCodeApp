﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EfFirstCodeApp
{
    public class MyContext : DbContext
    {
        public DbSet<Form> Forms { set; get; }
        public DbSet<FormItem> FormItems { set; get; }
        public DbSet<FormItemOverhaul> FormItemOverhauls { set; get; }
        public DbSet<FormItemOptimization> FormItemOptimizations { set; get; }
        public DbSet<FormRevision> FormRevisions { set; get; }
        public DbSet<User> Users { set; get; }
        public DbSet<Login> Logins { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Section> Sections { set; get; }
        public DbSet<Message> Messages { set; get; }
        public DbSet<Recipient> Recipients { set; get; }
        public DbSet<PowerCompany> PowerCompanies { set; get; }
        public DbSet<PowerPlant> PowerPlants { set; get; }
        public DbSet<PowerUnit> PowerUnits { set; get; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<FormItemOptimization>().ToTable("FormItemOptimizations");
            modelBuilder.Entity<FormItemOverhaul>().ToTable("FormItemOverhauls");

            

            //User
            modelBuilder.Entity<User>().Property(u => u.UserName).HasMaxLength(50).IsRequired().HasColumnType("VARCHAR");
            modelBuilder.Entity<User>().Property(u => u.Password).HasMaxLength(50).IsRequired().HasColumnType("VARCHAR");
            modelBuilder.Entity<User>().Property(u => u.FullName).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<User>().Property(u => u.Email).HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.PhoneNo).HasMaxLength(50);

            //Login
            modelBuilder.Entity<Login>().Property(l => l.Time).IsRequired();
            modelBuilder.Entity<Login>().Property(l => l.InOut).IsRequired();
            modelBuilder.Entity<Login>().Property(l => l.IP).HasMaxLength(20).IsFixedLength();

            //PowerCompany
            modelBuilder.Entity<PowerCompany>().Property(p => p.Code).HasMaxLength(20).IsRequired().HasColumnType("VARCHAR");
            modelBuilder.Entity<PowerCompany>().Property(p => p.Name).HasMaxLength(100).IsRequired();
            //PowerPlant
            modelBuilder.Entity<PowerPlant>().Property(p => p.Code).HasMaxLength(20).IsRequired().HasColumnType("VARCHAR");
            modelBuilder.Entity<PowerPlant>().Property(p => p.Name).HasMaxLength(100).IsRequired();
            //PowerUnit
            modelBuilder.Entity<PowerUnit>().Property(p => p.UnitCode).HasMaxLength(5).IsRequired().HasColumnType("VARCHAR");

            //Section
            modelBuilder.Entity<Section>().Property(s => s.Title).HasMaxLength(50).IsRequired();

            //Comment
            modelBuilder.Entity<Comment>().Property(s => s.Text).HasMaxLength(2000).IsRequired();
            modelBuilder.Entity<Comment>().Property(s => s.AttachmentFilePath).HasMaxLength(1000).IsRequired();

            //Form

            //FormItem
            modelBuilder.Entity<FormItem>().Property(f => f.Title).HasMaxLength(500).IsRequired();
            modelBuilder.Entity<FormItem>().Property(f => f.Notes).HasMaxLength(1000);
            //FormItemOverhaul
            modelBuilder.Entity<FormItemOverhaul>().Property(f => f.UnitCodes).HasMaxLength(100);
            //FormRevision
            modelBuilder.Entity<FormRevision>().Property(f => f.Title).HasMaxLength(50);

            //Message
            modelBuilder.Entity<Message>().Property(m => m.Title).HasMaxLength(200).IsRequired();
            modelBuilder.Entity<Message>().Property(m => m.Body).HasMaxLength(2000);
      

            //modelBuilder.Entity<FormItemOptimization>().Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable("FormItemOptimizations");
            //});
            //modelBuilder.Entity<FormItemOverhaul>().Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable("FormItemOverhauls");
            //});
        }

    }
}
