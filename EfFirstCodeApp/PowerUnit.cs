﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class PowerUnit
    {
        
        public int Id{ get; set; }

        //[StringLength(5)]
        //[Column(TypeName = "VARCHAR")]
        public string UnitCode { get; set; }

        public int PowerPlantId { get; set; }
        public virtual PowerPlant PowerPlant { get; set; }
    }

    
}
