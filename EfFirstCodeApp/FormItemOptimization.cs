﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace EfFirstCodeApp
{
    public class FormItemOptimization : FormItem
    {
  
        public int? LastYearApprovedCreditRial { get; set; }
        public int? LastYearApprovedCreditDollar { get; set; }
        public int? LastYearRevisedCreditRial { get; set; }
        public int? LastYearRevisedCreditDollar { get; set; }
        public int? LastYearCompletePercent { get; set; }
        public int? ThisYearCompletePercent { get; set; }
        public int? LastYearPerformanceRial { get; set; }
        public int? LastYearPerformanceDollar { get; set; }
        public int? ThisYearPerformanceRial { get; set; }
        public int? ThisYearPerformanceDollar { get; set; }
    }
}
