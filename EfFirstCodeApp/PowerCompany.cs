﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class PowerCompany
    {
        public int PowerCompanyId { get; set; }
        public PowerCompany(string code,string name) { Code = code; Name = name; }
        
      
        //[StringLength(20), Required]        
        //[Column(TypeName = "VARCHAR")]
        public string Code { get; set; }

        //[StringLength(200),Required]
        public string Name { get; set; }

        public ICollection<PowerPlant> PowerPlants { get; set; }
    }

   
}
