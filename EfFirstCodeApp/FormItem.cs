﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class FormItem
    {
        public int Id { get; set; }
   
        
        public int Row { get; set; }
        public int OriginalRow { get; set; }
        //[StringLength(500)]
        public string Title { get; set; }
        //public User Creator { get; set; }
        //[StringLength(20),Required]
        //[Column(TypeName = "VARCHAR")]
        public DateTime CreateDateTime { get; set; }
        public FormItemStatus Status { get; set; }
        //[StringLength(1000)]
        public string Notes { get; set; }

        //Revised and Approved Credit Proposed 

        public int? ApprovedCreditRial { get; set; }
        public int? ApprovedCreditDollar { get; set; }
        public int? RevisedCreditRial { get; set; }
        public int? RevisedCreditDollar { get; set; }
        public int? NextYearProposedCreditRial { get; set; }
        public int? NextYearProposedCreditDollar { get; set; }
        public Section Section { get; set; }

        public int FormRevisionId { get; set; }
        public virtual FormRevision FormRevision { get; set; }
    }
}
