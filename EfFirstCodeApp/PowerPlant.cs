﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class PowerPlant
    {
        public PowerPlant()
        {
        }
        public PowerPlant(string code,string name,PowerCompany company)
        {
            Code = code;
            Name = name;
            Company = company;
        }
       
        public int PowerPlantId { get; set; }
        
        //[StringLength(20), Required]       
        //[Column(TypeName = "VARCHAR")]
        public string Code { get; set; }

        //[StringLength(200), Required]        
        public string Name { get; set; }

        public int PowerCompanyId { get; set; }
        //[Required]
        public virtual PowerCompany Company { get; set; }
        public ICollection<PowerUnit> Units { get; set; }
    }

    
}
