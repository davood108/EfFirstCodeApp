﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EfFirstCodeApp
{
    public class Section
    {
        public int SectionId { get; set; }
       
        //[StringLength(50),Required]
        public string Title { get; set; }
    }
}
