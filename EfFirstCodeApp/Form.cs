﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class Form
    {
        public int FormId { get; set; }
       // [Required]
        public int Year { get; set; }
        public PowerCompany Company { get; set; }
        public PowerPlant PowerPlant { get; set; }        
       // [Required]
        public User Creator { get; set; }
        //[StringLength(20),Required]
        //[Column(TypeName = "VARCHAR")]
        public DateTime CreateDateTime { get; set; }
        public FormType Type { get; set; }
        public FormStatus Status { get; set; }
        public ICollection<FormRevision> FormRevisions { get; set; }



    }

    public enum FormStatus
    {
        WaitingSupervisor1,
        WaitingSections,
        WaitingCompany1,
        WaitingSession,
        WaitingCompany2,
        WaitingSupervisor2,
        FinalConfirm        
    }
}
