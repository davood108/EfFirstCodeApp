﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class FormRevision
    {
        public int FormRevisionId { get; set; }
        //[Required]
       
        public int Revision { get; set; }
        //[StringLength(50)]
        public string Title { get; set; }

        //[Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        //[StringLength(20),Required]
        //[Column(TypeName = "VARCHAR")]
        public DateTime CreateDateTime { get; set; }
        public ICollection<FormItem> FormItems { get; set; }
        //public List<FormItemOptimization> FormItemOptimizations { get; set; }
        //public List<FormItemOverhaul> FormItemOverhauls { get; set; }
        public int FormId { get; set; }
        public virtual Form Form { get; set; }
    }
}
