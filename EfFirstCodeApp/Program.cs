﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EfFirstCodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MyContext ctx = new MyContext())
            {
                //bool lazy=ctx.Configuration.LazyLoadingEnabled;
                //ctx.Database.Log = Console.Write;            
                ctx.Database.Log = sql => System.Diagnostics.Debug.Write(sql);

                PowerUnit uu2 = ctx.PowerUnits.SingleOrDefault(xxx => xxx.UnitCode == "G11");//.Include("PowerPlant")
                                                                                             //uu2.PowerPlantId = 2;
                                                                                             // uu2.PowerPlant = ctx.PowerPlants.SingleOrDefault(powerPlant => powerPlant.PowerPlantId == 2);
                                                                                             // ctx.SaveChanges();

                uu2 = ctx.PowerUnits.SingleOrDefault(xxx => xxx.UnitCode == "G11");
                var v = ctx.PowerPlants.Find(2).Units.ToList();
                PowerPlant powerPlantt = uu2.PowerPlant;
                Console.WriteLine(uu2.PowerPlant.Code);

                User u = new User { UserName = "admin2", Role = UserRole.TpphSupervisor, Password = "1", FullName = "فرجی", CreateDateTime = DateTime.Now }; // ctx.Users.SingleOrDefault(x => x.UserName == "admin");//

                PowerCompany c = new PowerCompany("1", " برق تهران");
                PowerPlant pp = new PowerPlant("101", "بعثت", c);
                PowerUnit uu = new PowerUnit() { UnitCode = "G11", PowerPlant = pp };// PowerPlantId = 1,
                ctx.PowerUnits.Add(uu);


                Form f = new Form { PowerPlant = pp, Type = FormType.Overhaul, Year = 1398, CreateDateTime = DateTime.Now, Creator = u };//ctx.Forms.SingleOrDefault(x => x.Id == 2);

                //FormRevision fr =new FormRevision() { Form = f, Revision = 1, Creator = u };

                FormRevision fr = new FormRevision() { Revision = 1, User = u, CreateDateTime = DateTime.Now };
                f.FormRevisions = new List<FormRevision>();
                f.FormRevisions.Add(fr);

                FormItemOverhaul record = new FormItemOverhaul { Title = "باتری", ApprovedCreditDollar = 100, ApprovedCreditRial = 1000000, Row = 1, OriginalRow = 1, Status = FormItemStatus.Unchanged, CreateDateTime = DateTime.Now };// Section = Section.Mechanics
                FormItemOverhaul record2 = new FormItemOverhaul { Title = " 2 توربین", ApprovedCreditDollar = 150, ApprovedCreditRial = 1000000, Row = 2, OriginalRow = 2, Status = FormItemStatus.Unchanged, CreateDateTime = DateTime.Now };//Section = Section.Chemistry
                FormItemOverhaul record3 = new FormItemOverhaul { Title = "UPS", ApprovedCreditDollar = 150, ApprovedCreditRial = 1000000, Row = 3, OriginalRow = 3, Status = FormItemStatus.Unchanged, CreateDateTime = DateTime.Now };// Section = Section.Chemistry 
                fr.FormItems = new List<FormItem>();
                fr.FormItems.Add(record);
                fr.FormItems.Add(record2);
                fr.FormItems.Add(record3);


                Form f2 = new Form { PowerPlant = pp, Type = FormType.Optimization, Year = 1398, CreateDateTime = DateTime.Now, Creator = u };//ctx.Forms.SingleOrDefault(x => x.Id == 2);            
                FormRevision fr2 = new FormRevision() { Revision = 1, User = u, CreateDateTime = DateTime.Now };//Form = f,
                f2.FormRevisions = new List<FormRevision>();
                f2.FormRevisions.Add(fr2);

                FormItemOptimization record4 = new FormItemOptimization { Title = "باتری", LastYearCompletePercent = 50, ApprovedCreditDollar = 100, ApprovedCreditRial = 1000000, Row = 1, OriginalRow = 1, Status = FormItemStatus.Edited, CreateDateTime = DateTime.Now };// Section = Section.Mechanics
                FormItemOptimization record5 = new FormItemOptimization { Title = " 2 توربین", LastYearPerformanceDollar = 2000, ApprovedCreditDollar = 150, ApprovedCreditRial = 1000000, Row = 2, OriginalRow = 2, Status = FormItemStatus.Edited, CreateDateTime = DateTime.Now };// Section = Section.Chemistry
                FormItemOptimization record6 = new FormItemOptimization { Title = "پره توربین", ThisYearCompletePercent = 10, ApprovedCreditDollar = 150, ApprovedCreditRial = 1000000, Row = 3, OriginalRow = 4, Status = FormItemStatus.Added, CreateDateTime = DateTime.Now };// Section = Section.Chemistry
                fr2.FormItems = new List<FormItem>();
                fr2.FormItems.Add(record4);
                fr2.FormItems.Add(record5);
                fr2.FormItems.Add(record6);

                ctx.Forms.Add(f);
                ctx.Forms.Add(f2);

                //ctx.FormRecords.Add(record);
                //ctx.FormRecords.Add(record2);
                //ctx.FormRecords.Add(record3);


                //ctx.FormRecords.Add(record3);
                //ctx.FormRecords.Add(record4);
                //ctx.FormRecords.Add(record5);
                ctx.SaveChanges();



                Console.WriteLine(ctx.Forms.Count());
                Console.WriteLine(ctx.Database.Connection.ConnectionString);

                string s = Console.ReadLine();

                var yy = ctx.Forms.SingleOrDefault(x => x.FormId == 1);
                var xx1 = ctx.Forms.SingleOrDefault(x => x.FormId == 1).FormRevisions;
                var xx2 = ctx.Forms.SingleOrDefault(x => x.FormId == 1).FormRevisions.SingleOrDefault(y => y.Revision == 1);
                var items = ctx.Forms.SingleOrDefault(x => x.FormId == 1).FormRevisions.SingleOrDefault(y => y.Revision == 1).FormItems;
                var items2 = ctx.Forms.SingleOrDefault(x => x.FormId == 2).FormRevisions.SingleOrDefault(y => y.Revision == 1).FormItems;
                //var items2 = ctx.Forms.SingleOrDefault(x => x.Id == 1).FormRevisions.SingleOrDefault(y => y.Revision == 1).FormItemOverhauls;
                //chetor FormItemOptimizations ra bazyabi konim?

                Message m = new Message() { Title = "Hi" };
                Recipient r1 = new Recipient();
            }


        }
    }
}
