﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class User
    {
        public int UserId { get; set; }

        //[Column(TypeName = "VARCHAR")]
        //[StringLength(50), Required]      
        public string UserName { get; set; }

        //[StringLength(50), Required, DataType(DataType.Password)]
        //[Column(TypeName = "VARCHAR")]           
        public string Password { get; set; }

       
        //[StringLength(100), Required]        
        public string FullName { get; set; }

        public bool IsActive { get; set; }

        //[StringLength(20)]
        //[Column(TypeName = "VARCHAR")]
        public DateTime CreateDateTime { get; set; }

        public UserRole Role { get; set; }

        public int? PowerCompanyId { get; set; }
        public virtual PowerCompany Company { get; set; }

        public int? SectionId { get; set; }
        public virtual Section Section { get; set; }

        //[StringLength(100), DataType(DataType.EmailAddress)]      
        public string Email { get; set; }

        //[StringLength(50), DataType(DataType.PhoneNumber)]        
        public string PhoneNo { get; set; }

        public ICollection<Login> LoginHistory { get; set; }

    }
    public enum UserRole
    {
        Admin,
        System,
        Section,
        PowerCompany,
        TpphSupervisor,
        TpphManager
    }
}
