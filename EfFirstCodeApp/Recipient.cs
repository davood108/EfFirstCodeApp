﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class Recipient
    {
        public Recipient()
        {
            Read = false;
            IsDeleted = false;
        }
        public int RecipientId { get; set; }

        public int? UserId { get; set; }//To avoid multiple cascade path error on sql
        //[Required]
        public virtual User RecipientUser { get; set; }
        //[StringLength(20)]
        //[Column(TypeName = "VARCHAR")]
        public DateTime ReadDateTime { get; set; }
        public bool Read { get; set; }
        public bool IsDeleted { get; set; }

        public int MessageId { get; set; }
        public virtual Message Message { get; set; }

    }

    
}
