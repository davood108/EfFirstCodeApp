﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfFirstCodeApp
{
    public class Message
    {
        public Message()
        {
            IsDeleted = false;
        }
        public int MessageId { get; set; }
       
        //[StringLength(200),Required]
        public string Title { get; set; }
       //[StringLength(2000)]
        public string Body { get; set; }
        public int UserId { get; set; }
        //[Required]
        public virtual User Sender { get; set; }
        //[StringLength(20),Required]
        //[Column(TypeName = "VARCHAR")]
        public DateTime SentDateTime { get; set; }      
        public Form RealtedForm { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Recipient> Recipients { get; set; }

    }

    
}
